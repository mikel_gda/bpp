function solutionArray = solveBPP(feasibleSubsets)
% Given the set of feasible subsets, get solutions for the BPP

    % get total number of feasible subsets and items
    [nSubsets, nItems] = size(feasibleSubsets);

    % initialize solution array
    solutionArray = zeros(nItems);
   
    % find BPP solutions
    aux = zeros(1,nItems);
    row = 1;
    for i=1:nSubsets
        % see if an item is in two different containers
        asignedItems = (aux + feasibleSubsets(i,:));
        if all(asignedItems < 2)
            % feasibleSubset(i) is part of the solution
            solutionArray(row,:) = feasibleSubsets(i,:);
            % reinitialize auxiliary vars
            aux = asignedItems;
            row = row + 1;
        end
        
        %if all items asigned return solution
        if sum(sum(solutionArray,1)) == nItems
            solutionArray = solutionArray(1:row-1,:);
            break
        end

    end

end
        


% def obtainSolution(feasibleSubsets):
%     fillingArray = np.zeros((len(feasibleSubsets[0]),), dtype=int)
%     solutionArrays = []
%     solution = []
% 
%     for i in range(len(feasibleSubsets)):
%         #print(feasibleSubsets[i], totalWeights[i])
% 
%         if (np.amax([x + y for x, y in zip(fillingArray, feasibleSubsets[i])]) < 2):
%             solution.append(i)
%             fillingArray = [x + y for x, y in zip(fillingArray, feasibleSubsets[i])]
%         if (np.amin(fillingArray) > 0):
%             break
% 
%     #print(fillingArray)
%     for ii in range(len(solution)):
%         solutionArrays.append(feasibleSubsets[solution[ii]])
% 
%     if (np.amin(fillingArray) == 0):
%         return 0
%     else:
%         return solutionArrays