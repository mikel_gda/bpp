function [totalWeights, feasibleSubsets] = feasibleSubsets(subsets_path, nItems, weights, capacity)
% feasibleSubsets cleans results from subsets_data according to data BPP
% weigts and capacity.
    
% load subsets file
subsets = readmatrix(subsets_path);

% compute the total weight for every subset
totalWeightSubset = sum(weights'.*subsets,2);

% identify subsets meeting condition totalWeight <= C
feasibleIndx = totalWeightSubset <= capacity;
nFeasible = sum(feasibleIndx);

% feasible subests and corresponding weigths
feasibleSubsets = zeros(nFeasible + nItems, nItems);
totalWeights = zeros(nFeasible + nItems, 1);

% feasible solutions from subsets
feasibleSubsets(1:nFeasible,:) = subsets(feasibleIndx,:);
totalWeights(1:nFeasible) = totalWeightSubset(feasibleIndx);

% add trivial solution (each item in a continer) to feasibleSubsets
feasibleSubsets(nFeasible+1:end,:) = eye(nItems);
totalWeights(nFeasible+1:end) = weights;

% order feasibleWeights according to totalWeights
[totalWeights, sortIndx] = sort(totalWeights, 'descend');
feasibleSubsets = feasibleSubsets(sortIndx, :);

end