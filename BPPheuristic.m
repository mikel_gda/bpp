function [BPPsolutions, nContainers] = BPPheuristic(feasibleSubsets)
% Given possible cointainer solutions (feasibleSubsets), this script
% computes solutions to the BPP problem

% initialize variable for full BPP solutions
BPPsolutions = {};
nContainers = 10000000000;

% compute results
nSubsets = length(feasibleSubsets);
for i=1:nSubsets
    sol = solveBPP(feasibleSubsets(i:end,:));
    if any(sol)
        BPPsolutions{end+1,1} = sol;
        if nContainers > size(sol,1)
            nContainers = size(sol,1);
        end
    end
end

end