%% INSTACE
clear;
BPPinstancePath = 'instances/Instance3.txt';
BPPsubsetsPath = 'subsets/subsetsResults_Instance3.txt';

[nItems, weights, capacity] = readInstanceData(BPPinstancePath);


%% Solve BPP using a brute force algorithm
[BF_optNcontainers, BF_sols, BF_Nsols] = BPPbruteforce(nItems,weights,capacity);

%% Generate subsets using DQA
TotalRuns = 1000;
beta = min(weights)/5;
h0cte = 10;
T = 10^14;
nT = 500;
subsetSampler(BPPinstancePath,BPPsubsetsPath,TotalRuns,beta,h0cte,T,nT);

%% Solve BPP using a heuristic algorithm
[totalWeights, feasibleSubsets] = feasibleSubsets(BPPsubsetsPath, nItems, weights, capacity);
nFeasibleSubsets = length(totalWeights);
[HEUR_sols, HEUR_optNcontainers] = BPPheuristic(feasibleSubsets);

%% BPP brute force vs BPP heursitic
disp('----- MAIN RESULTS -----')
disp('1) Optimal number of containers')
fprintf('    BF :: %d \n', BF_optNcontainers)
fprintf('    Heuristic :: %d \n', HEUR_optNcontainers)
disp('2) Number of optimal solutions')
fprintf('    BF :: %d \n', length(BF_sols))
aux = 0;
for i=1:length(HEUR_sols)
    if size(HEUR_sols{i},1) == HEUR_optNcontainers
        aux = aux + 1;
    end
end
fprintf('    Heuristic :: %d \n', aux)

% fprintf('----- END -----')

%% OBTAIN UNIQUE SUBSETS FROM BF_sols
A = [];
for i=1:BF_Nsols
    BF_solSubsets = vec2mat(BF_sols{i});
    for m=1:BF_optNcontainers
        A(end+1,:) = BF_solSubsets(m,:);
    end
end
[C,iA,ic] = unique(A,'rows');
counts = accumarray(ic,1);
[counts, icounts] = sort(counts, 'descend');
BF_uniqueSubsets = [C(icounts,:), counts];

disp('----- ANALYSIS OF THE HEURISTIC SOLVER -----')
fprintf('1) Num. diff. subsets required for computing BF solutions :: %d \n', size(BF_uniqueSubsets,1))

%% USING THE HEURISTIC SOLVER AND BF_uniqueSubets GET SOLUTIONS FOR THE BPP
totalWeights_BFunique = sum(weights' .* BF_uniqueSubsets(:,1:end-1), 2);
[~, indx] = sort(totalWeights_BFunique, 'descend');
[HEUR_BFsols, HEUR_BFoptNcontainers] = BPPheuristic(BF_uniqueSubsets(indx,1:end-1));

disp('2) RESULTS USING THE HEURISTIC SOLVER AND BF_uniqueSubets')
fprintf('    Opt. Num. Containers :: %d \n', HEUR_BFoptNcontainers)
fprintf('    Num. of solutions computed :: %d (%.3f%%) \n', length(HEUR_BFsols), length(HEUR_BFsols)/BF_Nsols)
% fprintf('----- END -----')

%% FIND COMMON SUBSETS IN BF_uniqueSubsets & FeasibleSubsets

% initialize
common_subsets = []; % common subsets in feasibleSubsets & BF_sols
missing_subsets = []; % missing subsets in feasibleSubsets

for i=1:size(BF_uniqueSubsets,1)
    for j=1:nFeasibleSubsets
        if all(feasibleSubsets(j,:) == BF_uniqueSubsets(i,1:end-1))
            common_subsets(end+1,:) = BF_uniqueSubsets(i,:);
            break
        elseif j == nFeasibleSubsets
            missing_subsets(end+1,:) = BF_uniqueSubsets(i,:);
        end
    end
end
disp('----- ANALYSIS OF THE SAMPLE GENERATOR -----')
fprintf('Number of common subsets feasibe/bf :: %d (%.3f%%) \n', size(common_subsets,1), (size(common_subsets,1)/size(BF_uniqueSubsets,1)))
fprintf('Number of missing subsets in feasible :: %d (%.3f%%) \n', size(missing_subsets,1), (size(missing_subsets,1)/size(BF_uniqueSubsets,1)))

fig = figure;
subplot(2,1,1); bar(common_subsets(:,end)/BF_Nsols); ylim([0, 1]); grid on;
subplot(2,1,2); bar(missing_subsets(:,end)/BF_Nsols); ylim([0, 1]); grid on;
han=axes(fig,'visible','off'); 
han.Title.Visible='on';
han.XLabel.Visible='on';
han.YLabel.Visible='on';
ylabel(han,'Freq in BF opt. sols.');
xlabel(han,'Subsets ID.');
title(han,'Common & Missing subsets freq');

%% BRUTE FORCE USING COMMON SUBSETS
disp('Following sols cam be computed from feasibleSubsets')
counter = 0;
for i=1:BF_Nsols
    BF_solSubsets = vec2mat(BF_sols{i});
    aux = 0;
    for c=1:BF_optNcontainers
        for j=1:nFeasibleSubsets
            if all(BF_solSubsets(c,:) == feasibleSubsets(j,:))
                aux=aux+1;
                break
            end
        end
    end
    if aux == BF_optNcontainers
        disp(BF_sols{i})
        counter = counter + 1;
    end
end
fprintf('Total :: %d \n', counter)

