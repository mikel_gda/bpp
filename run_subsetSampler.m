% Script to generate the subsets of all the instances
clear all

for loop = 1:10
for instance = 1:18    
    
    disp("START INSTANCE "+instance+" (LOOP "+loop+")")
    
    BPPinstancePath = "instances/instance"+instance+".txt";
    BPPsubsetsPath = "subsets/subsetsResults_instance"+instance+"_"+loop+".txt";

    [nItems, weights, capacity] = readInstanceData(BPPinstancePath);
    
    % Generate subsets using DQA
    TotalRuns = 2000;
    beta = min(weights)/5;
    h0cte = 10;
    T = 10^14;
    nT = 100;
    thres = 1/100;
    subsetSampler(BPPinstancePath,BPPsubsetsPath,TotalRuns,thres,beta,h0cte,T,nT);
end
end
