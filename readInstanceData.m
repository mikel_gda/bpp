function [nItems, weights, capacity] = readInstanceData(instance_path)
% readInstanceData loads a BPP instace defined in instance_path

    % Read instance file
    fileID = fopen(instance_path, 'r');
    lines = fscanf(fileID,'%d');
    fclose(fileID);
    
    % return BPP data
    nItems = lines(1);
    capacity = lines(2);
    weights = lines(3:end);

end