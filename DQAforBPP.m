function probs = DQAforBPP(C,w,alpha,beta,h0cte,T,nT)
% DQAforBPP launches a simulation for the digital annealing algorithm solving BPP.
% The annealing scheduling function we are using is linear, Lambda(t)=t/T. This saves us computational time.
%   C           Capacity of the container
%   w           Weights of the packages as a vector of reals
%   alpha       Solution "tolerance". For alpha=0 we are aiming at exact solutions, while for alpha=2C we aim at the 00...0 result. 0 <= alpha <= 2betaC
%   beta        "Searching needle width". For beta->0 we aim at obtaining containers with total weight C-alpha/2. beta > 0
%   h0cte       Scaling of the initial Hamiltonian
%   T           Annealing time
%   nT          Number of steps for the discretization
%
%   probs       Returns vector of the probability distribution of measuring each posible state (ordered as [00 01 10 11] and so on)

% Generate some useful parameters
deltaT = T/nT; %width of a time step 
hbar = 6.582119e-16; %\hbar in eV*s
rW = -C+sum(w)/2; %constant that goes into the problem Hamiltonian
N = length(w); %number of packages 

% Generate the problem Hamiltonian
Hp = zeros(1,2^N);
z = [1 -1]; %diagonal of the pauli z matrix
% Local terms
for j = 1:N
    Hp = Hp - w(j)*(alpha/2+beta*rW)    *kron(ones(1,2^(j-1)),kron(z,ones(1,2^(N-j))));
end
% Interacting terms
for j1 = 1:N-1
    for j2 = j1+1:N
        Hp = Hp + beta*w(j1)*w(j2)/2    *kron(ones(1,2^(j1-1)),kron(z,kron(ones(1,2^(j2-j1-1)),kron(z,ones(1,2^(N-j2))))));
    end
end

% Calculate the initial ground state
state = 1;
for i = 1:N
    state = kron([1/sqrt(2) -1/sqrt(2)],state);
end
state = state'; %transpose the vector so that we have a column vector

% Digitized Quantum annealing simulation (noiseless)
for i = 1:nT-1
    % Since H0 acts the same in every qubit, we can use this trick to easily obtain the evolution matrix
    % Generate the evolution matrix for one qubit
    H0block=[cos(deltaT*h0cte*(nT-i)/(2*hbar*nT)) -1i*sin(deltaT*h0cte*(nT-i)/(2*hbar*nT)); -1i*sin(deltaT*h0cte*(nT-i)/(2*hbar*nT)) cos(deltaT*h0cte*(nT-i)/(2*hbar*nT))];
    % Construct the full evolution matrix by calculating the tensor product
    fullH0block=[1];
    for k=1:N
        fullH0block=kron(H0block,fullH0block);
    end
    state = fullH0block*state;
    state = diag(exp(-1i*deltaT*i*Hp/(hbar*nT)))*state;
    state = fullH0block*state;
end

% Calculate the probability of obtaining each vector of the basis
probs = abs(conj(state).*state);


end

