function res = subsetSampler(instance,output,TotalRuns,thres,beta,h0cte,T,nT)
% Lauch the simulation to solve an instance
%  instance   File name of the instance file, with the following codification:
%              The first row has the total number of elements
%              The second row has the capacity of the container
%              The rest of the rows has the weight of each element
%  output     File name of the output file
%  thres      Fraction threshold for the algorithm to stop searching container with a total weight less than C*thres 
%  TotalRuns  Number of total runs for the DQA algorithm for the sumbet sampling
%  alpha      Solution "tolerance". For alpha=0 we are aiming at exact solutions, while for alpha=2C we aim at the 00...0 result. 0 <= alpha <= 2betaC
%  beta       "Searching needle width". For beta->0 we aim at obtaining containers with total weight C-alpha/2. beta > 0
%  h0cte      Scaling of the initial Hamiltonian
%  T          Annealing time
%  nT         Number of steps for the discretization
%
% Returns a matrix in witch each row represent a sampled valid subset

% Extract the problem parameters from the text file
fileID = fopen(instance,'r');
raw = fscanf(fileID,'%d');
fclose(fileID);
N = raw(1);
C = raw(2);
w = raw(3:end)';

% Objetive container weights
minDiff = C;
for i = 1:length(w)-1
    for j = 2:length(w)
        if minDiff > abs(w(i)-w(j)) && w(i) ~= w(j)
            minDiff = abs(w(i)-w(j));
        end
    end
end
Wobj = C:-minDiff:ceil(C*thres/minDiff)*minDiff;

% Distribute the runs
if rem(sum(w),C)==0
    % We look harder to find perfect fits
    runs = floor(TotalRuns/(length(Wobj)+1))*ones(1,length(Wobj));
    runs(1) = runs(1)+TotalRuns-sum(runs);
else
    runs = floor(TotalRuns/(length(Wobj)))*ones(1,length(Wobj));
    runs(1) = runs(1)+TotalRuns-sum(runs);
end
    

% Launch the algorithm for different values of alpha
res = [];
for j = 1:length(Wobj)
    % Calculate the new alpha value
    alpha = 2*beta*(C-Wobj(j));
    
    % Call to the simulation
    if isequal(gpuDevice().Name,'NVIDIA GeForce RTX 2070 SUPER')
        tic
        probs = DQAforBPP_GPU(C,w,alpha,beta,h0cte,T,nT);
        toc
    else
        tic
    	probs = DQAforBPP(C,w,alpha,beta,h0cte,T,nT);
        toc
    end
    
    % Simulate diferent measurements
    for i = 1:runs(j)
        % Weighted random sample
        meas = randsample(2^N,1,true,probs);
        % Convert the measurement output to string
        meas = dec2bin(meas-1,N);
        meas = str2num(replace(meas,'',' ')); 
        % Add the measurement to the result list
        res(end+1,:) = meas;
    end
    
    % Delete repeated solutions
    res = unique(res,'rows');
end

%Delete repeated solutions
res = unique(res,'rows');

% Write the subsets on a file
fileO = fopen(output,"w");
for line = 1:height(res)
    for column = 1:width(res)
        fprintf(fileO,"%2d",res(line,column));
    end
    fprintf(fileO,"\r\n");
end
fclose(fileO);


end

