function [nContainers, bruteForceSols, nOptimalSols] = BPPbruteforce(nItems, weights, capacity)
%% ENEKO'S BRUTE FORCE 

maxContainer = nItems;
minContainer = sum(weights > (0.5*capacity));

for n=minContainer:maxContainer

    stop=false;
    
    % get all possible combinations
    aux = strcat(mat2str(1:n),',');
    aux = repmat(aux, 1, nItems);
    aux = aux(1:end-1);
    combinations = eval(strcat('allcomb(', aux ,')'));

    % allocate brute force solutions here
    bruteForceSols = {};

    % evaluate solutions for n containers
    for i=1:n^nItems
        for j=1:n
            containerWeight = sum(weights(combinations(i,:)==j));
            if containerWeight > capacity
                break;
            end
            if j == n
                bruteForceSols{end+1} = combinations(i,:);
                stop=true;
            end
        end
        % if stop
        %    display(bruteForceSol)
        %    break
        % end
    end

    % if we find at least 1 solution for n containers end search 
    if stop
        % optimal number of containers
        nContainers = n;
        % number of optimal solutions
        nOptimalSols = size(bruteForceSols,2);
        break
    end
end

end





