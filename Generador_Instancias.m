% Instancias gaussianas centradas en 1/2 de C
instance = [10,100,single_gaussian_packages(10,50,20,10,100)]';
save_instance("instances\instance1.txt",instance)
instance = [10,120,single_gaussian_packages(10,60,24,10,120)]';
save_instance("instances\instance2.txt",instance)
instance = [10,150,single_gaussian_packages(10,75,30,10,150)]';
save_instance("instances\instance3.txt",instance)
instance = [12,100,single_gaussian_packages(12,50,20,10,100)]';
save_instance("instances\instance4.txt",instance)
instance = [12,120,single_gaussian_packages(12,60,24,10,120)]';
save_instance("instances\instance5.txt",instance)
instance = [12,150,single_gaussian_packages(12,75,30,10,150)]';
save_instance("instances\instance6.txt",instance)

% Instancias de 2 gaussianas centradas en 1/3 y 2/3 de C
instance = [10,100,two_gaussian_packages(10,33,10,66,10,0.5,10,100)]';
save_instance("instances\instance7.txt",instance)
instance = [10,120,two_gaussian_packages(10,40,12,80,12,0.5,10,120)]';
save_instance("instances\instance8.txt",instance)
instance = [10,150,two_gaussian_packages(10,50,15,100,15,0.5,10,150)]';
save_instance("instances\instance9.txt",instance)
instance = [12,100,two_gaussian_packages(12,33,10,66,10,0.5,10,100)]';
save_instance("instances\instance10.txt",instance)
instance = [12,120,two_gaussian_packages(12,40,12,80,12,0.5,10,120)]';
save_instance("instances\instance11.txt",instance)
instance = [12,150,two_gaussian_packages(12,50,15,100,15,0.5,10,150)]';
save_instance("instances\instance12.txt",instance)

% Instancias uniformes
instance = [10,100,uniform_packages(10,10,100)]';
save_instance("instances\instance13.txt",instance)
instance = [10,120,uniform_packages(10,10,120)]';
save_instance("instances\instance14.txt",instance)
instance = [10,150,uniform_packages(10,10,150)]';
save_instance("instances\instance15.txt",instance)
instance = [12,100,uniform_packages(12,10,100)]';
save_instance("instances\instance16.txt",instance)
instance = [12,120,uniform_packages(12,10,120)]';
save_instance("instances\instance17.txt",instance)
instance = [12,150,uniform_packages(12,10,150)]';
save_instance("instances\instance18.txt",instance)


function save_instance(name,instance)
fileID = fopen(name,'w');
fprintf(fileID,"%d\r\n",instance);
fclose(fileID);
end

function plotear(instance)
inc = [];
for i = 0:10:instance(2)
    inc(end+1) = sum(instance(3:end)==i);
end
bar(0:10:instance(2),inc)
end

function res = single_gaussian_packages(n,centro,sigma,diffMin,C)
% n         número de paquetes
% centro    centro de la distribución
% sigma     anchura de la distribución
% diffMin   minima diferencia entre paquetes
% C         capacidad del container

res = [];
while length(res) < n
    aux = round(normrnd(centro,sigma));   
    if mod(aux,diffMin) < diffMin/2
        aux = aux - mod(aux,diffMin);
    else
        aux = aux + diffMin - mod(aux,diffMin);
    end 
    if aux > 0 && aux < C
        res(end+1) = aux;    
    end
end
end

function res = two_gaussian_packages(n,centro1,sigma1,centro2,sigma2,prob1,diffMin,C)
% n         número de paquetes
% centro    centro de la distribución
% sigma     anchura de la distribución
% prob1     probabilidad de que un paquete caiga en la primera gaussiana
% diffMin   minima diferencia entre paquetes
% C         capacidad del container
res = [];
while length(res) < n
    if rand() < prob1
        aux = round(normrnd(centro1,sigma1));  
    else
        aux = round(normrnd(centro2,sigma2));
    end
    if mod(aux,diffMin) < diffMin/2
        aux = aux - mod(aux,diffMin);
    else
        aux = aux + diffMin - mod(aux,diffMin);
    end
    if aux > 0 && aux < C
        res(end+1) = aux;  
    end
end
end

function res = uniform_packages(n,diffMin,C)
% n         número de paquetes
% diffMin   minima diferencia entre paquetes
% C         capacidad del container
res = zeros(1,n);
for i = 1:n
    res(i) = randsample(diffMin:diffMin:C-1,1);
end
end