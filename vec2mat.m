function mat = vec2mat(vec)
% vec2mat converts vec to a matrix of n rows and m columns, where n is the
% maximun interger number in vec and m the number of elemnts in vec.

% vector parameters
n = max(vec);
m = length(vec);

% initialize matrix
mat = zeros(n, m);

% convert vec into mat
for i=1:n
    mat(i,:) = (vec(:) == i);
end

end